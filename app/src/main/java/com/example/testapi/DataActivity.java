package com.example.testapi;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class DataActivity extends AppCompatActivity {
    private ListView listview;
    private List<String> listId=new ArrayList<String>();
    private List<String> listName=new ArrayList<String>();
    Button logout;
    TextView dataId;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data);
        Intent itLogout = new Intent(this, MainActivity.class);
        logout= findViewById(R.id.btt_logout);
        listview = (ListView) findViewById(R.id.listView);
        listview.setAdapter(new dataListAdapter(getApplicationContext()));
        //Bundle tex = getIntent().getExtras();
        listId = getIntent().getStringArrayListExtra("IdDataList");
        listName = getIntent().getStringArrayListExtra("nameDataList");
        //list.add(tex.getString("userId"));
        //  list.add(tex.getString("IdDataList"));
        // list.add(tex.getString("password"));
        Log.d("DataTag","listId    "+listId);
        Log.d("DataTag","listName    "+listName);
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
                startActivity(itLogout);

            }
        });
    }

    public class dataListAdapter  extends BaseAdapter {
        public Context mContext;
        public LayoutInflater mInflater;

        public dataListAdapter(Context context) {
            mContext = context;
            mInflater = LayoutInflater.from(mContext);

        }

        @Override
        public int getCount() {

            return listId.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if(convertView==null)
            {
                //load laout
                convertView = mInflater.inflate(R.layout.activity_list,null);
                    dataId = convertView.findViewById(R.id.textView);
                convertView.setTag(dataId);

            }else{
                //rebind
                dataId = (TextView) convertView.getTag();
            }
                dataId.setText((position + 1) + ">>" + listId.get(position) + "\n" + listName.get(position));
            return convertView;

        }
    }
}