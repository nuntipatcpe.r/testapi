package com.example.testapi;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;




public class MainActivity extends AppCompatActivity {

    JSONArray dataGuest;
    String userToken;
    TextView getUserText, getPasswordText;

    int responseCodeRequestMain = 0;
    int responseCodePassTokenMain = 0;

public  MainActivity() {

}

    ArrayList<String> nameDataList = new ArrayList<>();
    ArrayList<String> IdDataList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button btt_1 = findViewById(R.id.button);
        TextView username = findViewById(R.id.editTextTextUserName);
        TextView password = findViewById(R.id.editTextTextPassword);
        TextView status = findViewById(R.id.tv_status);
        Intent intentPutToDataActivity = new Intent(this, DataActivity.class);

        btt_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String  usrBBS="http://business-bulletin-service.co.th:8080/thena/api/v1/_thena_user/";
                getUserText = username;
                getPasswordText = password;

                Log.d("MainTag", "In userId  :"+ getUserText.getText());

                    while (true) {
                        testTask task = new testTask();
                        try {
                            if (userToken == null) {
                                task.execute("authenticate", "POST", usrBBS).get();
                                if( responseCodeRequestMain != 200) {
                                    status.setText("Login failed");
                                    Log.d("MainTag", "---Not Response Code Request---" );
                                    break;
                                }

                            }else {
                                task.execute(" ", "GET", usrBBS).get();
                                Log.d("MainTag", "In userId  :" + IdDataList);
                                Log.d("MainTag", "In userName  :" + nameDataList);
                                intentPutToDataActivity.putStringArrayListExtra("IdDataList", IdDataList);
                                intentPutToDataActivity.putStringArrayListExtra("nameDataList", nameDataList);
                                finish();
                                startActivity(intentPutToDataActivity);
                                break;
                            }
                        } catch (ExecutionException e) {
                            e.printStackTrace();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }

            }
        });
    }

    private class testTask extends AsyncTask<String,Void,Void> {
        String urls,met, bbsUrl;
        int responseCodeRequest = 0;
        int responseCodePassToken = 0;
        PipeData dataJson =new PipeData();
        
        @Override
        protected Void doInBackground(String...urls) {
            try {
                this.urls = urls[0];
                this.met = urls[1];
                this.bbsUrl = urls[2];

                URL obj = new URL(this.bbsUrl+this.urls);
                HttpURLConnection con = (HttpURLConnection) obj.openConnection();

                if(userToken==null) {
                    con.setRequestMethod(this.met);//POSS
                    // con.setDoOutput(true);
                    //con.setRequestProperty("Content-Type", "application/json");
                    // this.responseCode = con.getResponseCode();
                    // System.out.println("GET Response Code :: " + responseCode);

                    JSONObject json = new JSONObject();
                    try {
                        json.put("userId", getUserText.getText());
                        json.put("password", getPasswordText.getText());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
//                    DataOutputStream out = new DataOutputStream(con.getOutputStream());
//                    BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(out, "UTF-8"));
//                    writer.write(String.valueOf(json));
//                    writer.flush();
//                    writer.close();
//                    out.close();
//
                    dataJson.dataOutPut(con,json);
                    this.responseCodeRequest = con.getResponseCode();
                    MainActivity.this.responseCodeRequestMain = con.getResponseCode();
                }else
                {
                    con.setRequestMethod(this.met);
                    con.setRequestProperty("Authorization","Bearer "+userToken);
                    this.responseCodePassToken = con.getResponseCode();
                    MainActivity.this.responseCodePassTokenMain = con.getResponseCode();
                }
                    if(this.responseCodeRequest == HttpURLConnection.HTTP_OK && userToken==null) {
//                        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
//                        String inputLine;
//                        StringBuffer response = new StringBuffer();
//                        while ((inputLine = in.readLine()) != null) {
//                            response.append(inputLine);
//                        }
//                        in.close();
                       // JSONObject jsonInput = new JSONObject(String.valueOf(response));
                       // userToken = (String) jsonInput.get("token");
                       MainActivity.this.userToken = this.dataJson.inPutReturnToString(con,"token");
                        Log.d("MainTag", "userToken  :"+userToken);
                    }else if (this.responseCodePassToken==HttpURLConnection.HTTP_OK&&userToken!=null)
                    {
//                        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
//                        String inputLine;
//                        StringBuffer response = new StringBuffer();
//                        while ((inputLine = in.readLine()) != null) {
//                            response.append(inputLine);
//                        }
//                        in.close();
//                        JSONObject dataGuest = new JSONObject(String.valueOf( response));
                        //MainActivity.this.dataGuest =  dataGuest.getJSONArray("data");
                        MainActivity.this.dataGuest =  this.dataJson.inPutReturnToJSONArray(con,"data");
                        JSONObject temp=null;
                        for(int i=0;i<MainActivity.this.dataGuest.length();i++) {
                            temp = (JSONObject) MainActivity.this.dataGuest.get(i);
                            IdDataList.add("Id : "+temp.get("userId"));
                            String firstLastName = "Firstname : "+temp.get("firstname")+" "+"Lastname : "+temp.get("lastname");
                            nameDataList.add(firstLastName);
                        }
                    }
                    Log.d("MainTag", "ResponseCode Request:  " + this.responseCodeRequest);
                    Log.d("MainTag", "ResponseCode PassToken:  " + this.responseCodePassToken);

            } catch (Exception e){
                Log.e("MyTag",e.toString());
            }

            return null;

        }
        @Override
        protected void onProgressUpdate(Void... values) {
           // super.onProgressUpdate(values);
        }
        @Override
        protected void onPostExecute(Void unused) {
            Log.d("MainTag", "____________________________________________________");
            Log.d("MainTag", "ResponseCode Request:  " + this.responseCodeRequest);
            Log.d("MainTag", "ResponseCode PassToken:  " + this.responseCodePassToken);



        }

        private void getToken(){


        }


    }
}