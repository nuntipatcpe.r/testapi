package com.example.testapi;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;

public class PipeData {
    HttpURLConnection con;
    JSONArray dataGuest;
    JSONObject jsonDataOutPut;
    String data,token;

    public JSONArray inPutReturnToJSONArray(HttpURLConnection con , String data) throws IOException, JSONException {
    this.con = con;
       this.data = data;
       BufferedReader in = new BufferedReader(new InputStreamReader( this.con.getInputStream()));
       String inputLine;
       StringBuffer response_2 = new StringBuffer();
       while ((inputLine = in.readLine()) != null) {
           response_2.append(inputLine);
       }
       in.close();
       JSONObject dataGuest = new JSONObject(String.valueOf( response_2));
       this.dataGuest=dataGuest.getJSONArray(this.data);
       return   this.dataGuest;
   }
    public String inPutReturnToString(HttpURLConnection con , String data) throws IOException, JSONException {
        this.con = con;
        this.data = data;
        BufferedReader in = new BufferedReader(new InputStreamReader( this.con.getInputStream()));
        String inputLine;
        StringBuffer response_2 = new StringBuffer();
        while ((inputLine = in.readLine()) != null) {
            response_2.append(inputLine);
        }
        in.close();
        JSONObject dataGuest = new JSONObject(String.valueOf( response_2));
        token = (String) dataGuest.get(this.data);
        return   token;
    }
    public void dataOutPut(HttpURLConnection con ,JSONObject jsonDataOutPut) throws IOException {
        this.jsonDataOutPut = jsonDataOutPut ;
        DataOutputStream out = new DataOutputStream(con.getOutputStream());
        BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(out, "UTF-8"));
        writer.write(String.valueOf(jsonDataOutPut));
        writer.flush();
        writer.close();
        out.close();

    }


}
