package com.example.testapi;


import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.clearText;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.matcher.ViewMatchers.isClickable;
import static androidx.test.espresso.matcher.ViewMatchers.withId;

import androidx.test.ext.junit.rules.ActivityScenarioRule;
import androidx.test.ext.junit.runners.AndroidJUnit4;



import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(AndroidJUnit4.class)
public class LoginTestEss {

    @Rule
        public ActivityScenarioRule<MainActivity> login = new ActivityScenarioRule<MainActivity>(MainActivity.class);
       // public ActivityScenarioRule<DataActivity> data = new ActivityScenarioRule<DataActivity>(DataActivity.class);




    @Test
    public void testLoginFailed(){

            onView(withId(R.id.editTextTextUserName)).perform(clearText());
            onView(withId(R.id.editTextTextPassword)).perform(clearText());
            onView(withId(R.id.editTextTextUserName)).perform(typeText("bbbbb"), closeSoftKeyboard());
            onView(withId(R.id.editTextTextPassword)).perform(typeText("aaaaa"), closeSoftKeyboard());
            onView(withId(R.id.button)).perform(click());

            onView(withId(R.id.editTextTextUserName)).perform(clearText());
            onView(withId(R.id.editTextTextPassword)).perform(clearText());
            onView(withId(R.id.editTextTextUserName)).perform(typeText("ergergwefwe"), closeSoftKeyboard());
            onView(withId(R.id.editTextTextPassword)).perform(typeText("ergerger@wef"), closeSoftKeyboard());
            onView(withId(R.id.button)).perform(click());

            onView(withId(R.id.editTextTextUserName)).perform(clearText());
            onView(withId(R.id.editTextTextPassword)).perform(clearText());
            onView(withId(R.id.editTextTextUserName)).perform(typeText("vvvvw"), closeSoftKeyboard());
            onView(withId(R.id.editTextTextPassword)).perform(typeText("hertheth"), closeSoftKeyboard());
            onView(withId(R.id.button)).perform(click());

    }

    @Test
    public void testLoginScuess(){
        onView(withId(R.id.editTextTextUserName)).perform(clearText());
        onView(withId(R.id.editTextTextPassword)).perform(clearText());
        onView(withId(R.id.editTextTextUserName)).perform(typeText("guest"),closeSoftKeyboard());
        onView(withId(R.id.editTextTextPassword)).perform(typeText("guest"),closeSoftKeyboard());
        onView(withId(R.id.button)).perform(click());
        onView(withId(R.id.btt_logout)).perform(click());

        onView(withId(R.id.editTextTextUserName)).perform(clearText());
        onView(withId(R.id.editTextTextPassword)).perform(clearText());
        onView(withId(R.id.editTextTextUserName)).perform(typeText("aa"),closeSoftKeyboard());
        onView(withId(R.id.editTextTextPassword)).perform(typeText("aa"),closeSoftKeyboard());
        onView(withId(R.id.button)).perform(click());
        onView(withId(R.id.btt_logout)).perform(click());

        onView(withId(R.id.editTextTextUserName)).perform(clearText());
        onView(withId(R.id.editTextTextPassword)).perform(clearText());
        onView(withId(R.id.editTextTextUserName)).perform(typeText("bb"),closeSoftKeyboard());
        onView(withId(R.id.editTextTextPassword)).perform(typeText("bb"),closeSoftKeyboard());
        onView(withId(R.id.button)).perform(click());
    }


}
